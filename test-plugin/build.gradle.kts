dependencies {

    annotationProcessor("org.projectlombok:lombok:1.18.12")

    compileOnly("org.spigotmc:spigot-api:1.13.2-R0.1-SNAPSHOT")

    implementation(project(":processor"))
    ksp(project(":processor"))

}

tasks {
    jar {

        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        from (
            configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) }
        )

    }

}
