package me.fortibrine.justapi.annotations

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Plugin (
    val name: String,
    val version: String,
)
