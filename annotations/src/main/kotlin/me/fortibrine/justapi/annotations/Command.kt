package me.fortibrine.justapi.annotations

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Command (
    val name: String,
    val usage: String = "",
    val description: String = ""
)