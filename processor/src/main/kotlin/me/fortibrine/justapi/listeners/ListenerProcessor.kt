package me.fortibrine.justapi.listeners

import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.*
import com.google.devtools.ksp.validate
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ksp.toClassName
import com.squareup.kotlinpoet.ksp.writeTo
import me.fortibrine.justapi.annotations.Listener
import me.fortibrine.justapi.annotations.Plugin

class ListenerProcessor(
    private val codeGenerator: CodeGenerator,
) : SymbolProcessor {

    private val visitor = ListenerVisitor(codeGenerator)
    private lateinit var plugin: KSClassDeclaration
    private val codeBlockBuilder = CodeBlock.builder()

    override fun process(resolver: Resolver): List<KSAnnotated> {
        val symbols = resolver.getSymbolsWithAnnotation(Listener::class.qualifiedName.orEmpty())
        val validatedSymbols = symbols.filter(KSNode::validate).toList()

        val pluginSymbols = resolver.getSymbolsWithAnnotation(Plugin::class.qualifiedName.orEmpty())
        val validatedPluginSymbols = pluginSymbols.filter(KSNode::validate).toList()

        if (validatedPluginSymbols.isNotEmpty()) {
            plugin = validatedPluginSymbols.first() as KSClassDeclaration
        }

        validatedSymbols.forEach {
            it.accept(visitor, codeBlockBuilder)
        }

        return pluginSymbols.toList() + symbols.toList() - validatedSymbols.toSet() - validatedPluginSymbols.toSet()
    }

    override fun finish() {


        FileSpec.builder(plugin.toClassName().packageName, "Listeners")
            .addType(
                TypeSpec.classBuilder("Listeners")
                    .primaryConstructor(
                        FunSpec.constructorBuilder()
                            .addParameter("plugin", plugin.toClassName())
                            .build()
                    )
                    .addProperty(
                        PropertySpec.builder("plugin", plugin.toClassName())
                            .addModifiers(KModifier.PRIVATE)
                            .initializer("plugin")
                            .build()
                    )
                    .addInitializerBlock(
                        codeBlockBuilder.build()
                    )
                    .build()
            )
            .build()
            .writeTo(codeGenerator, true)
    }

}
