package me.fortibrine.justapi.listeners

import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSNode
import com.google.devtools.ksp.visitor.KSDefaultVisitor
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.MemberName
import com.squareup.kotlinpoet.ksp.toClassName

class ListenerVisitor(
    private val codeGenerator: CodeGenerator
): KSDefaultVisitor<CodeBlock.Builder, Unit>() {

    override fun defaultHandler(node: KSNode, data: CodeBlock.Builder) {}

    override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: CodeBlock.Builder) {

        val bukkit = ClassName("org.bukkit", "Bukkit")
        val getPluginManager = MemberName(bukkit, "getPluginManager")

        data.addStatement("%M().registerEvents(%T(), plugin)", getPluginManager, classDeclaration.toClassName())

    }

}