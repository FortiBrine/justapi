package me.fortibrine.justapi.commands

import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSNode
import com.google.devtools.ksp.visitor.KSDefaultVisitor
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ksp.toClassName
import me.fortibrine.justapi.utils.getArgument

class CommandVisitor(
    private val codeGenerator: CodeGenerator
): KSDefaultVisitor<Unit, MutableMap<String, ClassName>>() {

    override fun defaultHandler(node: KSNode, data: Unit): MutableMap<String, ClassName> {
        return mutableMapOf()
    }

    override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit):  MutableMap<String, ClassName> {
        val annotation = classDeclaration.annotations.first { it.shortName.getShortName() == "Command" }

        val name = annotation.getArgument<String>("name")

        return mutableMapOf(name to classDeclaration.toClassName())

    }

}