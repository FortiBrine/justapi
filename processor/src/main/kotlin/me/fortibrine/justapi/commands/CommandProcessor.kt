package me.fortibrine.justapi.commands

import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.*
import com.google.devtools.ksp.validate
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ksp.toClassName
import com.squareup.kotlinpoet.ksp.writeTo
import me.fortibrine.justapi.annotations.Command
import me.fortibrine.justapi.annotations.Plugin

class CommandProcessor(
    private val codeGenerator: CodeGenerator,
) : SymbolProcessor {

    private lateinit var plugin: KSClassDeclaration
    private val visitor: CommandVisitor = CommandVisitor(codeGenerator)
    private val commands: MutableMap<String, ClassName> = mutableMapOf()

    override fun process(resolver: Resolver): List<KSAnnotated> {
        val symbols = resolver.getSymbolsWithAnnotation(Command::class.qualifiedName.orEmpty())
        val validatedSymbols = symbols.filter(KSNode::validate).toList()

        val pluginSymbols = resolver.getSymbolsWithAnnotation(Plugin::class.qualifiedName.orEmpty())
        val validatedPluginSymbols = pluginSymbols.filter(KSNode::validate).toList()

        if (validatedPluginSymbols.isNotEmpty()) {
            plugin = validatedPluginSymbols.first() as KSClassDeclaration
        }

        validatedSymbols.forEach {
            commands.putAll(it.accept(visitor, Unit))
        }

        return pluginSymbols.toList() + symbols.toList() - validatedSymbols.toSet() - validatedPluginSymbols.toSet()
    }

    override fun finish() {

        val codeBlockBuilder = CodeBlock.builder()

        commands.forEach { commandName, className ->
            codeBlockBuilder
                .addStatement("plugin.getCommand(%S)?.setExecutor(%T())", commandName, className)
        }

        FileSpec.builder(plugin.toClassName().packageName, "Commands")
            .addType(
                TypeSpec.classBuilder("Commands")
                    .primaryConstructor(
                        FunSpec.constructorBuilder()
                            .addParameter("plugin", plugin.toClassName())
                            .build()
                    )
                    .addProperty(
                        PropertySpec.builder("plugin", plugin.toClassName())
                            .addModifiers(KModifier.PRIVATE)
                            .initializer("plugin")
                            .build()
                    )
                    .addInitializerBlock(
                        codeBlockBuilder.build()
                    )
                    .build()
            )
            .build()
            .writeTo(codeGenerator, true)
    }

}
