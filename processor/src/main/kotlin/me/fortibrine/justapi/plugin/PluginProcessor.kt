package me.fortibrine.justapi.plugin

import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.*
import com.google.devtools.ksp.validate
import com.squareup.kotlinpoet.ksp.toClassName
import me.fortibrine.justapi.annotations.Command
import me.fortibrine.justapi.annotations.Plugin
import me.fortibrine.justapi.utils.getArgument

class PluginProcessor(
    private val codeGenerator: CodeGenerator,
) : SymbolProcessor {

    private lateinit var plugin: KSClassDeclaration
    private val commands: MutableList<CommandData> = mutableListOf()
    private val visitor: CommandVisitor = CommandVisitor(codeGenerator)

    override fun process(resolver: Resolver): List<KSAnnotated> {
        val pluginSymbols = resolver.getSymbolsWithAnnotation(Plugin::class.qualifiedName.orEmpty())
        val validatedPluginSymbols = pluginSymbols.filter(KSNode::validate).toList()

        val commandSymbols = resolver.getSymbolsWithAnnotation(Command::class.qualifiedName.orEmpty())
        val validatedCommandSymbols = commandSymbols.filter(KSNode::validate).toList()

        if (validatedPluginSymbols.isNotEmpty()) {
            plugin = validatedPluginSymbols.first() as KSClassDeclaration
        }

        validatedCommandSymbols.forEach {
            commands.add(it.accept(visitor, Unit))
        }

        return pluginSymbols.toList() + commandSymbols.toList() - validatedPluginSymbols.toSet() - validatedCommandSymbols.toSet()
    }

    override fun finish() {

        val annotation = plugin.annotations.first {
            it.shortName.getShortName() == "Plugin"
        }

        val name = annotation.getArgument<String>("name")
        val version = annotation.getArgument<String>("version")

        val dependencies = Dependencies(true)
        codeGenerator.createNewFile(dependencies, "", "plugin.yml", "").bufferedWriter().use {
                writer ->
            writer.write("name: $name\n")
            writer.write("main: ${plugin.toClassName()}\n")
            writer.write("version: $version\n")

            if (commands.isNotEmpty()) {
                writer.write("commands:\n")
            }

            commands.forEach {
                writer.write("    ${it.name}:\n")
                if (it.usage != null) {
                    writer.write("        usage: ${it.usage}\n")
                }
                if (it.description != null) {
                    writer.write("        description: ${it.description}\n")
                }
            }

            writer.close()
        }
    }

}
