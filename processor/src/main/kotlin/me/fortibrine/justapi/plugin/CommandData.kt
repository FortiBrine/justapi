package me.fortibrine.justapi.plugin

data class CommandData constructor(
    val name: String,
    val usage: String? = null,
    val description: String? = null
) 