package me.fortibrine.justapi.plugin

import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSNode
import com.google.devtools.ksp.visitor.KSDefaultVisitor
import me.fortibrine.justapi.utils.getArgument

class CommandVisitor(
    private val codeGenerator: CodeGenerator
): KSDefaultVisitor<Unit, CommandData>() {

    override fun defaultHandler(node: KSNode, data: Unit): CommandData {
        return CommandData("")
    }

    override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit): CommandData {
        val annotation = classDeclaration.annotations.first { it.shortName.getShortName() == "Command" }

        val name = annotation.getArgument<String>("name")
        val usage = annotation.getArgument<String>("usage")
        val description = annotation.getArgument<String>("description")

        return CommandData(
            name = name,
            usage = if (usage == "") null else usage,
            description = if (description == "") null else description
        )

    }

}